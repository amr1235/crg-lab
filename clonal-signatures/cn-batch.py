import pandas as pd
from utils import *

def main():
    copy_numbers = read_copy_numbers("data/copy-number.seg")
    
    cols = ["Hugo_Symbol","Chromosome","Start_Position","End_Position","Variant_Classification","Variant_Type","ref","Tumor_Sample_Barcode","Matched_Norm_Sample_Barcode","alt","CONTEXT","Purity","pattern","trinucleotide","Total_Tumor_Copy_Number","Normal_Copy_Number","t_alt_count","t_ref_count"]
    with open("batches/cn-batch.maf","w") as f:
        f.write("\t".join(cols)+"\n")
    
    for tumors in pd.read_csv("batches/cnless-batch.maf.gz",sep="\t",header=0, chunksize=100000):
        print(tumors.shape)

        tumors = tumors.astype({"Chromosome": "int32", "Start_Position": "float", "End_Position": "float"}, copy=False)

        print("Adding Copy Numbers...")
        tumors = add_copy_numbers(copy_numbers, tumors)

        tumors = tumors[tumors["Total_Tumor_Copy_Number"] <= 2]

        tumors = tumors[tumors["Normal_Copy_Number"] <= 2]

        tumors = tumors[tumors["Total_Tumor_Copy_Number"] > 0]

        tumors = tumors[tumors["Normal_Copy_Number"] > 0]


        print("Writing " + str(tumors.shape[0]) + " mutations to file...")
        tumors.to_csv("batches/cn-batch.maf", sep="\t", mode="a", header=False, index=False,columns=cols)

if __name__ == "__main__":
    main()
