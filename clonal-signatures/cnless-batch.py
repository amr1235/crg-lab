import pandas as pd
from utils import *

def main():
    cols = ["Hugo_Symbol","Tumor_Sample_Barcode","Matched_Norm_Sample_Barcode","Variant_Classification","Variant_Type","Chromosome","Start_Position","End_Position","CONTEXT","Allele","Reference_Allele","t_ref_count","t_alt_count"]
    tumors = pd.read_csv("data/mrms.maf.gz",sep="\t", header=0, usecols=cols)
    purities = read_purities("data/purity.txt")
    copy_numbers = read_copy_numbers("data/copy-number.seg")
    
    # preprocessing
    tumors["Tumor_Sample_Barcode"] = tumors["Tumor_Sample_Barcode"].str.slice(0,15)
    tumors["Matched_Norm_Sample_Barcode"] = tumors["Matched_Norm_Sample_Barcode"].str.slice(0,15)
    tumors["Chromosome"] = tumors["Chromosome"].replace(["X", "Y"], "23")
    tumors = tumors.astype({"Chromosome": "int32", "Start_Position": "float", "End_Position": "float"}, copy=False)
    
    # filtering
    copy_numbers = copy_numbers[copy_numbers["Sample"].isin(list(tumors["Tumor_Sample_Barcode"].unique()) + list(tumors["Matched_Norm_Sample_Barcode"].unique()))]
    available_cns = copy_numbers["Sample"].unique()
    tumors = tumors[tumors["Tumor_Sample_Barcode"].isin(available_cns)]
    tumors = tumors[tumors["Matched_Norm_Sample_Barcode"].isin(available_cns)]
    
    
    print("Picking SNPS...")
    tumors = pick_snps(tumors)

    print("Adding Purity...")
    tumors = add_purity(purities, tumors)

    print("Adding Context...")
    tumors = add_trinucleotide_context(tumors)
    
    print("Writing " + str(tumors.shape[0]) + " cnless mutations to file...")
    tumors.to_csv("batches/cnless-batch.maf.gz", sep="\t", mode="w", header=True, index=False)

if __name__ == "__main__":
    main()
