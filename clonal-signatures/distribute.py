import pandas as pd
from multiprocessing import Pool,Lock
import time
import logging
from utils import clonal_sigs, get_tumor_mutations

logging.basicConfig(filename="clonesig.log",filemode="a",level=20,format="%(asctime)s:%(process)d:%(message)s")
l = Lock()

def set_clones(muts):
    l.acquire()
    logging.info(str(muts["Tumor_Sample_Barcode"].values[0])+":started.")
    l.release()
    
    est, lr, pval, new_inputMU, cst_est, future_sigs = clonal_sigs(muts)
    muts["clone"]=est.qun.argmax(axis=1)
    
    l.acquire()
    muts.to_csv("batches/clonesig-batch.maf", sep="\t", mode="a", header=False, index=False)
    logging.info(str(muts["Tumor_Sample_Barcode"].values[0])+":ended.")
    l.release()


if __name__ == '__main__':
    cols = ["Hugo_Symbol","Chromosome","Start_Position","End_Position","Variant_Classification","Variant_Type","ref","Tumor_Sample_Barcode","Matched_Norm_Sample_Barcode","alt","CONTEXT","Purity","pattern","trinucleotide","Total_Tumor_Copy_Number","Normal_Copy_Number","t_alt_count","t_ref_count"]
    with open("batches/clonesig-batch.maf","w") as f:
        f.write("\t".join(cols)+"\n")

    pool = Pool(processes=4)
    itr = get_tumor_mutations(pd.read_csv("batches/cn-batch.maf.gz",sep="\t",chunksize=1,header=0))

    for mut in itr:
        pool.apply_async(set_clones, (mut,))

# screen -S clonesig
# id=92116
# source .venv/bin/activate
# python distribute.py
# ctrl a d
