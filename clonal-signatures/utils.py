import numpy as np
import pandas as pd
from clonesig.run_clonesig import get_MU, run_clonesig
from clonesig.data_loader import get_context, PAT_LIST

def get_tumor_mutations(itr):
    rows = next(itr)
    cur = rows["Tumor_Sample_Barcode"].values[0]
    for row in itr:
        if cur == row["Tumor_Sample_Barcode"].values[0]:
            cur = row["Tumor_Sample_Barcode"].values[0]
            rows = rows.append(row)
        else:
            cur = row["Tumor_Sample_Barcode"].values[0]
            cpy = rows.copy()
            rows = pd.DataFrame([],columns=row.columns)
            rows = rows.append(row)
            yield cpy
    yield rows


def read_purities(filename):
    data = []
    with open(filename) as f:
      for line in f.readlines():
        line = line.split("\t")
        data.append([line[0], line[3]])
    return pd.DataFrame(data[1:], columns=["Tumor_Sample_Barcode", "Purity"])

def read_copy_numbers(filename):
    cn = pd.read_csv(filename,sep="\t", header=0)
    cn["Sample"] = cn["Sample"].str.slice(0,15)
    cn["Copy_Number"] = 2 * (2 ** cn["Segment_Mean"])
    cn = cn.astype({"Chromosome": "int32", "Start": "float", "End": "float", "Copy_Number": "int32"}, copy=False)
    return cn

def add_purity(purities, muts):
    return pd.merge(muts, purities, how="inner", on="Tumor_Sample_Barcode")

def pick_snps(muts):
    return muts[muts["Variant_Type"] == "SNP"]

def add_trinucleotide_context(muts):
    # rename to match column names in get_context
    muts.rename(columns={"Reference_Allele":"ref","Allele":"alt"},inplace=True)
    muts.astype({"CONTEXT": "str","ref": "str", "alt": "str"}, copy=False)
    muts = muts.assign(
        pattern=pd.Categorical(
            muts.apply(get_context, axis=1),
            categories=PAT_LIST, ordered=True))
    return muts.assign(
        trinucleotide=muts.apply(lambda x: PAT_LIST.index(x['pattern']), axis=1))

def add_copy_numbers(copy_numbers, tumors):
    t = pd.merge(tumors, copy_numbers,left_on="Tumor_Sample_Barcode",right_on="Sample")
    n = pd.merge(tumors, copy_numbers,left_on="Matched_Norm_Sample_Barcode",right_on="Sample")
    t = t[(t["Chromosome_x"] == t["Chromosome_y"]) & (t["Start_Position"] >= t["Start"]) & (t["End_Position"] <= t["End"])]
    n = n[(n["Chromosome_x"] == n["Chromosome_y"]) & (n["Start_Position"] >= n["Start"]) & (n["End_Position"] <= n["End"])]
    t.rename(columns={"Copy_Number":"Total_Tumor_Copy_Number"},inplace=True)
    return pd.merge(t,n[["Tumor_Sample_Barcode","Matched_Norm_Sample_Barcode","Chromosome_x","Start_Position","End_Position","Copy_Number"]],on=["Tumor_Sample_Barcode","Matched_Norm_Sample_Barcode","Chromosome_x","Start_Position","End_Position"]).rename(columns={"Chromosome_x":"Chromosome","Copy_Number":"Normal_Copy_Number"},copy=False)

def clonal_sigs(muts):
    return run_clonesig(
    T=muts["trinucleotide"].astype(int).values,
    B=muts["t_alt_count"].astype(int).values,
    D=muts["t_ref_count"].astype(int).values + muts["t_alt_count"].astype(int).values,
    C_normal=muts["Normal_Copy_Number"].astype(int).values,
    C_tumor_tot=muts["Total_Tumor_Copy_Number"].astype(int).values,  
    C_tumor_minor=np.zeros(muts["Tumor_Sample_Barcode"].shape),
    purity=muts["Purity"].values[0],
    inputMU=get_MU(cosmic_version=2))