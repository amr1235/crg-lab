import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from utils import *

def main():
    fields = ["Tumor_Sample_Barcode","Hugo_Symbol","Variant_Classification"]
    repair_genes = pd.read_csv("data/repair_genes_pathways.csv",header=0)["Hugo_Symbol"].values
    tumors = pd.read_csv("tumors.maf.gz","gzip",sep="\t",usecols=fields, header=0)

    mrms =  pd.read_csv("mrms.maf.gz", sep="\t", header=0, usecols=fields)
    nonmrms =  pd.read_csv("nonmrms.maf.gz", sep="\t", header=0, usecols=fields)

    one_df, two_df = tumors_with_one_and_two_mrms(repair_genes, tumors, mrms)

    combinations = combine_means(one_df, two_df)

    basal_mean = nonmrms.groupby(["Tumor_Sample_Barcode"])["Tumor_Sample_Barcode"].count().mean()
    combinations_diff = pd.DataFrame([],columns=["mrm1", "mrm2", "diff"])
    combinations_diff["mrm1"] = combinations["mrm1"]
    combinations_diff["mrm2"] = combinations["mrm2"]
    combinations_diff["diff"] = (combinations["count_in_combined"] - combinations["count_in_mrm1"] - combinations["count_in_mrm2"] + basal_mean).astype(np.float)
    # multiple tumors may have the same combination so we take the mean
    combinations_diff = combinations_diff.groupby(["mrm1","mrm2"])["diff"].mean().reset_index()

    genes = np.unique(np.append(combinations_diff["mrm1"].unique(),combinations_diff["mrm2"].unique()))
    grid = pd.DataFrame([], index=genes, columns=genes, dtype=np.float)
    for mrm in grid:
        # pick the mrm whether it's in the first or the second column
        temp = combinations_diff[(combinations_diff["mrm1"] == mrm) | (combinations_diff["mrm2"] == mrm)]
        for _, row in temp.iterrows():
            if row["mrm1"] == mrm:
                grid.loc[mrm, row["mrm2"]] = row["diff"]
            else:
                grid.loc[mrm, row["mrm1"]] = row["diff"]

    _, ax = plt.subplots(figsize=(50,50))
    sns.heatmap(grid, linewidths=0.5, ax=ax, linecolor="green")
    plt.savefig("heatmap.png")

if __name__ == "__main__":
    main()