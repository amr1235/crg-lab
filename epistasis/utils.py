import numpy as np
import pandas as pd

def tumors_with_one_and_two_mrms(repair_genes, tumors, mrms):
    grouped = mrms.groupby(["Tumor_Sample_Barcode"])
    one_mrm_tum=[]
    one_mrm_gene_name=[]
    one_mrm_count=[]
    two_mrm_tum=[]
    two_mrm_gene_name=[]
    two_mrm_count=[]
    mrm_tum = grouped["Hugo_Symbol"].unique().reset_index()
    muts_count = tumors.groupby(["Tumor_Sample_Barcode"])["Hugo_Symbol"].count().reset_index()

    for item in mrm_tum.values:
        values = np.where(np.isin(item[1],repair_genes))[0]
        if len(values) == 1:
            one_mrm_gene_name.append(item[1][values[0]])
            one_mrm_count.append(muts_count[muts_count["Tumor_Sample_Barcode"] == item[0]]["Hugo_Symbol"].iloc[0])
            one_mrm_tum.append(item[0])
        elif len(values) == 2:
            two_mrm_gene_name.append([item[1][values[0]],item[1][values[1]]])
            two_mrm_count.append(muts_count[muts_count["Tumor_Sample_Barcode"] == item[0]]["Hugo_Symbol"].iloc[0])
            two_mrm_tum.append(item[0])


    dict1 ={ 'Tumor_Sample_Barcode':one_mrm_tum,'Hugo_Symbol':one_mrm_gene_name, "Count": one_mrm_count }
    one_df = pd.DataFrame(dict1)
    dict2 ={ 'Tumor_Sample_Barcode':two_mrm_tum,'Hugo_Symbol':two_mrm_gene_name, "Count": two_mrm_count }
    two_df = pd.DataFrame(dict2)
    return one_df,two_df

def combine_means(one_df, two_df):
    combinations = pd.DataFrame([],columns=["count_in_combined", "mrm1", "mrm2", "count_in_mrm1","count_in_mrm2"])
    for row in two_df.iterrows():
        res1 = one_df[one_df["Hugo_Symbol"] == row[1]["Hugo_Symbol"][0]]
        res2 = one_df[one_df["Hugo_Symbol"] == row[1]["Hugo_Symbol"][1]]

        if res1.shape[0] != 0 and res2.shape[0] != 0:
            mean1 = res1["Count"].mean()
            mean2 = res2["Count"].mean()
            ls = [row[1]["Hugo_Symbol"][0], row[1]["Hugo_Symbol"][1]]
            ls.sort() # just to make sure that (mrm1, mrm2) and (mrm2, mrm1) are the same combination
            combinations = combinations.append({
            "count_in_combined": row[1]["Count"],
            "mrm1": ls[0],
            "mrm2": ls[1],
            "count_in_mrm1": mean1,
            "count_in_mrm2": mean2
            },ignore_index=True)
                
    return combinations
